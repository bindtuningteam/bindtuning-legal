<p class="alert alert-info"> Last updated: October 02, 2019</p>
<br>
These Terms and Conditions ("Terms", "Terms and Conditions") govern your relationship with bindtuning.com website (the "Service") operated by Bind Lda ("us", "we", or "our").

Please read these Terms and Conditions carefully before using the Service.

Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.

By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.

___
### Purchases

If you wish to purchase any product or service made available through the Service ("Purchase"), you may be asked to supply certain information relevant to your Purchase including, without limitation, your credit card number, the expiration date of your credit card, your billing address, and your shipping information. 

You represent and warrant that: (i) you have the legal right to use any credit card(s) or other payment method(s) in connection with any Purchase; and that (ii) the information you supply to us is true, correct and complete. 

By submitting such information, you grant us the right to provide the information to third parties for purposes of facilitating the completion of Purchases. 

We reserve the right to refuse or cancel your order at any time for certain reasons including but not limited to: product or service availability, errors in the description or price of the product or service, error in your order or other reasons. 

We reserve the right to refuse or cancel your order if fraud or an unauthorised or illegal transaction is suspected. 
___
### Availability, Errors and Inaccuracies

We are constantly updating our offerings of products and services on the Service. The products or services available on our Service may be mispriced, described inaccurately, or unavailable, and we may experience delays in updating information on the Service and in our advertising on other web sites.

We cannot and do not guarantee the accuracy or completeness of any information, including prices, product images, specifications, availability, services, setup guides and other documentation provided. We reserve the right to change or update information and to correct errors, inaccuracies, or omissions at any time without prior notice.

___
### Subscriptions

Some parts of the Service are billed on a subscription basis ("Subscription(s)"). You will be billed in advance on a recurring and periodic basis ("Billing Cycle"). Billing cycles are set on an annual basis, but may be adjusted if overage is detected before the cycle ends (i.e: if the number of real users is higher than the number of contracted users).

At the end of each Billing Cycle, your Subscription will automatically renew under the exact same conditions unless overage is detected (in which case we will contact you ahead of renewal to adjust your plan) or you cancel it or Bind Lda cancels it. You may cancel your Subscription renewal either through your online account management page or by contacting Bind Lda customer support team at <support@bindtuning.com>.

A valid payment method is required to complete the renewal process and ensure your usage of the subscription is compliant. Valid payment methods accepted include credit card or PayPal, should payment be processed online. You shall provide Bind Lda with accurate and complete billing information including full name, address, state, zip code, telephone number, and a valid payment method information. By submitting such payment information, you automatically authorize Bind Lda to charge all Subscription fees incurred through your account to any such payment instruments.

Should you opt to be billed manually, and pay by wire transfer instead, or should automatic billing fail to occur for any reason, Bind Lda will issue an electronic invoice indicating that you must proceed manually, within a certain deadline date, with the full payment corresponding to the billing period as indicated on the invoice.

Failing to complete payment within the renewal timeframe (1 month before renewal date) will lead to temporary suspension of product updates and access to support tickets. All installed versions of the products will remain functional if intention of renewal is clearly communicated to the Customer Success Team by email. 

Subscription renewal is mandatory to keep using any BindTuning products sold as yearly subscriptions. The customer (or the partner on customer behalf) shall be notified, by email, to the BindTuning account owner in which the products have been made available and directly in its BindTuning account, at least one month before renewal. A renewal proposal shall be presented by the Customer Success Team during the renewal period. 

Non-renewal of BindTuning yearly subscriptions makes it mandatory to uninstall and completely remove from your environment any previously deployed products from your environment. Uninstalled expired products may show error messages and cause mal-functioning of which BindTuning may not be accounted responsible. 

___
### Free Trial

Bind Lda may, at its sole discretion, offer a Subscription with a free trial for a limited period of time ("Free Trial").

You may be required to enter your billing information in order to sign up for the Free Trial.

At any time and without notice, Bind Lda reserves the right to (i) modify the terms and conditions of the Free Trial offer, or (ii) cancel such Free Trial offer.

___
### Changes

Bind Lda, in its sole discretion and at any time, may modify its licensing and pricing model. Any fee, plan or price change will become effective at the end of the then-current Billing Cycle.

Bind Lda will provide you with a reasonable prior notice of any change in Subscription fees to give you an opportunity to terminate your Subscription before such change becomes effective.

Your continued use of the Service after the Subscription fee change comes into effect constitutes your agreement to pay the modified Subscription fee amount.

___
### Refunds

Certain refund requests for Subscriptions may be considered by Bind Lda on a case-by-case basis and granted in sole discretion of Bind Lda.

___
### Accounts

When you create an account with us, you must provide us information that is accurate, complete, and current at all times. Failure to do so constitutes a breach of the Terms, which may result in immediate termination of your account on our Service.

You are responsible for safeguarding the password that you use to access the Service and for any activities or actions under your password, whether your password is with our Service or a third-party service.

You agree not to disclose your password to any third party. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account.

You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trade mark that is subject to any rights of another person or entity other than you without appropriate authorization, or a name that is otherwise offensive, vulgar or obscene.

___
### Intellectual Property

The Service and its original content, features and functionality are and will remain the exclusive property of Bind Lda and its licensors. The Service is protected by copyright, trademark, and other laws of both the Portugal and foreign countries. Our trademarks and trade dress may not be used in connection with any product or service without the prior written consent of Bind Lda.

___
### Links To Other Web Sites

Our Service may contain links to third-party web sites or services that are not owned or controlled by Bind Lda.

Bind Lda has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that Bind Lda shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.

We strongly advise you to read the terms and conditions and privacy policies of any third-party web sites or services that you visit.

___
### Termination

We may terminate or suspend your account and access to any subscription or product immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.

Upon termination, your right to use the Service will immediately cease. 

If you wish to terminate your account, you may simply discontinue using the Service.

If you wish to terminate your usage of a subscription based product, all previously installed versions of that products shall be uninstalled and completely removed from your environment. Uninstalled expired products may show error messages  and cause mal-functioning of which BindTuning may not be accounted responsible. 

___
### Limitation Of Liability

In no event shall Bind Lda, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any third party on the Service; (iii) any content obtained from the Service; and (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.

___
### Disclaimer

Your use of the Service is at your sole risk. The Service is provided on an "AS IS" and "AS AVAILABLE" basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance.

Bind Lda its subsidiaries, affiliates, and its licensors do not warrant that a) the Service will function uninterrupted, secure or available at any particular time or location; b) any errors or defects will be corrected; c) the Service is free of viruses or other harmful components; or d) the results of using the Service will meet your requirements.

___
### Governing Law

These Terms shall be governed and construed in accordance with the laws of Portugal, without regard to its conflict of law provisions.

Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.

___
### Changes

We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.

By continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.

___
### Contact Us

If you have any questions about these Terms, please contact us at: <a href="mailto:support@bindtuning.com" target="_blank" >support@bindtuning.com</a>