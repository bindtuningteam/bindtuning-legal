<p class="alert alert-info"> Last updated: January 16, 2018</p>
<br>
Please read this End-User License Agreement ("Agreement") carefully before clicking the "I Agree" button, downloading or using BindTuning products ("Application").

By clicking the "I Agree" button, downloading or using the Application, you are agreeing to be bound by the terms and conditions of this Agreement.

This Agreement is a legal agreement between you (either an individual or a single entity) and Bind Lda and it governs your use of the Application made available to you by Bind Lda.

If you do not agree to the terms of this Agreement, do not click on the "I Agree" button and do not download or use the Application.

The Application is licensed, not sold, to you by Bind Lda for use strictly in accordance with the terms of this Agreement.

___
### License

Subject to the terms of this Agreement, Bind Lda grants you a revocable, non-exclusive, non-transferable, non-assignable, limited license to download, install and use the Application strictly in accordance with the terms of this Agreement / solely for your internal use purposes.

___
### Restrictions

You agree not to, and you will not permit others to:

- license, sell, rent, lease, assign, distribute, transmit, host, outsource, disclose or otherwise commercially exploit the Application or make the Application available to any third party;

- Copy, in whole or in part, or use the Application for any purpose other than as permitted under the above section "License";

- modify, make derivative works of, disassemble, decrypt, reverse compile or reverse engineer any part of the Application; or

- remove, alter or obscure any proprietary notice (including any notice of copyright or trademark) of Bind Lda or its affiliates, partners, suppliers or the licensors of the Application.

__
### Intellectual Property

The Application, including without limitation all copyrights, patents, trademarks, trade secrets and other intellectual property rights, are and shall remain the sole and exclusive property of Bind Lda.

___
### Your Suggestions

Any feedback, comments, ideas, improvements or suggestions provided by you to Bind Lda with respect to the Application (collectively, "Suggestions") shall remain the sole and exclusive property of Bind Lda.

Bind Lda shall be free to use, copy, modify, publish, or redistribute the Suggestions for any purpose and in any way without any credit or any compensation to you.

___
### Modifications to Application

Bind Lda reserves the right to modify, suspend or discontinue, temporarily or permanently, the Application or any service to which it connects, with or without notice and without liability to you.

___
### Updates to Application

Bind Lda may from time to time provide enhancements or improvements to the features/functionality of the Application, which may include patches, bug fixes, updates, upgrades and other modifications ("Updates").

Updates may modify or delete certain features and/or functionalities of the Application. You agree that Bind Lda has no obligation to (i) provide any Updates, or (ii) continue to provide or enable any particular features and/or functionalities ofthe Application to you.

You further agree that all Updates will be (i) deemed to constitute an integral part of the Application, and (ii) subject to the terms and conditions of this Agreement.

____
### Third-Party Services

The Application may display, include or make available third-party content (including data, information, applications and other products services) or provide links to third-party websites or services ("Third-Party Services").

You acknowledge and agree that Bind Lda shall not be responsible for any Third-Party Services, including their accuracy, completeness, timeliness, validity, copyright compliance, legality, decency, quality or any other aspect thereof. Bind Lda does not assume and shall not have any liability or responsibility to you or any other person or entity for any Third-Party Services.

Third-Party Services and links thereto are provided solely as a convenience to you and you access and use them entirely at your own risk and subject to such third parties' terms and conditions.

___
### Privacy Policy

Bind Lda collects, stores, maintains, and shares information about you in accordance with its Privacy Policy, which is available at **http://bindtuning.com/legal/privacy**.
<br>By accepting this Agreement, you acknowledge that you hereby agree and consent to the terms and conditions of our Privacy Policy.

___
### Term and Termination

This Agreement shall remain in effect until terminated by you or Bind Lda.

Bind Lda may, in its sole discretion, at any time and for any or no reason, suspend or terminate this Agreement with or without prior notice.

This Agreement will terminate immediately, without prior notice from Bind Lda, in the event that you fail to comply with any provision of this Agreement. You may also terminate this Agreement by deleting the Application and all copies thereof from your mobile device or from your computer.

Upon termination of this Agreement, you shall cease all use of the Application and delete all copies of the Application from your mobile device or from your computer.

Termination of this Agreement will not limit any of Bind Lda's rights or remedies at law or in equity in case of breach by you of any of your obligations under the Agreement, and all terms under this Agreement (other than under "License")shall survive termination of this Agreement.

___
### Indemnification

You agree to indemnify and hold Bind Lda and its parents, subsidiaries, affiliates, officers, employees, agents, partners and licensors (if any) harmless from any claim or demand, including reasonable attorneys' fees, due to or arising out of your: (a) use of the Application; (b) violation of this Agreement or any law or regulation; or (c) violation of any right of a third party.

___
### No Warranties

**THE APPLICATION IS PROVIDED TO YOU "AS IS" AND "AS AVAILABLE" AND WITH ALL FAULTS AND DEFECTS WITHOUT WARRANTY OF ANY KIND. TO THE MAXIMUM EXTENT PERMITTED UNDER APPLICABLE LAW, BIND LDA, ON ITS OWN BEHALF AND ON BEHALF OF ITS AFFILIATES AND ITS AND THEIR RESPECTIVE LICENSORS AND SERVICE PROVIDERS, EXPRESSLY DISCLAIMS ALL WARRANTIES, WHETHER EXPRESS, IMPLIED, STATUTORY OR OTHERWISE, WITH RESPECT TO THE APPLICATION, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT, AND WARRANTIES THAT MAY ARISE OUT OF COURSE OF DEALING, COURSE OF PERFORMANCE, USAGE OR TRADE PRACTICE.** Without limitation to the foregoing, Bind Lda provides no warranty or undertaking, and makes no representation of any kind that the Application will meet your requirements, achieve any intended results, be compatible or work with any other software, applications, systems or services, operate without interruption, meet any performance or reliability standards or be error free or that any errors or defects can or will be corrected. Without limiting the foregoing, neither Bind Lda nor any Bind Lda provider makes any representation or warranty of any kind, express or implied: (i) as to the operation or availability of the Application, or the information, content, and materials or products included thereon; (ii) that the Application will be uninterrupted or error-free; (iii) as to the accuracy, reliability, or currency of any information or content provided through the Application; or (iv) that the Application, its servers, the content, or e-mails sent from or on behalf of Bind Lda are free of viruses, scripts, trojan horses, worms, malware, timebombs or other harmful components.

Some jurisdictions do not allow the exclusion of or limitations on implied warranties or the limitations on the applicable statutory rights of a consumer, so some or all of the above exclusions and limitations may not apply to you.

___
### Limitation of Liability

Notwithstanding any damages that you might incur, the entire liability of Bind Lda and any of its suppliers under any provision of this Agreement and your exclusive remedy for all of the foregoing shall be limited to the amount actually paid by you for the Application.

**TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL BIND LDA OR ITS SUPPLIERS BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT, OR CONSEQUENTIAL DAMAGES WHATSOEVER (INCLUDING, BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, FOR LOSS OF DATA OR OTHER INFORMATION, FOR BUSINESS INTERRUPTION, FOR PERSONAL INJURY, FOR LOSS OF PRIVACY ARISING OUT OF OR IN ANY WAY RELATED TO THE USE OF OR INABILITY TO USE THE APPLICATION, THIRD-PARTY SOFTWARE AND/OR THIRD-PARTY HARDWARE USED WITH THE APPLICATION, OR OTHERWISE IN CONNECTION WITH ANY PROVISION OF THIS AGREEMENT), EVEN IF BIND LDA OR ANY SUPPLIER HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES AND EVEN IF THE REMEDY FAILS OF ITS ESSENTIAL PURPOSE.**

Some states/jurisdictions do not allow the exclusion or limitation of incidental or consequential damages, so the above limitation or exclusion may not apply to you.

___
### Severability

If any provision of this Agreement is held to be unenforceable or invalid, such provision will be changed and interpreted to accomplish the objectives of such provision to the greatest extent possible under applicable law and the remaining provisions will continue in full force and effect.

___
### Waiver

Except as provided herein, the failure to exercise a right or to require performance ofan obligation under this Agreement shall not affect a party's ability to exercise such right or require such performance at any time thereafter nor shall be the waiver of a breach constitute waiver of any subsequent breach.

___
### For U.S. Government End Users

The Application and related documentation are "Commercial Items", as that term is defined under 48 C.F.R. §2.101, consisting of "Commercial Computer Software" and "Commercial Computer Software Documentation", as such terms are used under 48 C.F.R. §12.212 or 48 C.F.R. §227.7202, as applicable. In accordance with 48 C.F.R. §12.212 or 48 C.F.R. §227.7202-1 through 227.7202-4, as applicable, the Commercial Computer Software and Commercial Computer Software Documentation are being licensed to U.S. Government end users (a) only as Commercial Items and (b) with only those rights as are granted to all other end users pursuant to the terms and conditions herein.

___
### Export Compliance

You may not export or re-export the Application except as authorized by United States law and the laws of the jurisdiction in which the Application was obtained.

In particular, but without limitation, the Application may not be exported or re-exported (a) into or to a nation or a resident of any U.S. embargoed countries or (b) to anyone on the U.S. Treasury Department's list of Specially Designated Nationals or the U.S. Department of Commerce Denied Person's List or Entity List.

By installing or using any component of the Application, you represent and warrant that you are not located in, under control of, or a national or resident of any such country or on any such list.

___
### Amendments to this Agreement

Bind Lda reserves the right, at its sole discretion, to modify or replace this Agreement at any time. If a revision is material we will provide at least 30 days' notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.

By continuing to access or use our Application after any revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, you are no longer authorized to use the Application.

___
### Governing Law

The laws of Portugal, excluding its conflicts of law rules, shall govern this Agreement and your use of the Application. Your use of the Application may also be subject to other local, state, national, or international laws.

____
### Contact Information

If you have any questions about this Agreement, please contact us at **info@bindtuning.com.**

___
### Entire Agreement

The Agreement constitutes the entire agreement between you and Bind Lda regarding your use of the Application and supersedes all prior and contemporaneous written or oral agreements between you and Bind Lda.

You may be subject to additional terms and conditions that apply when you use or purchase other Bind Lda's services, which Bind Lda will provide to you at the time of such use or purchase.